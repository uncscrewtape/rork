#define seed_menu
if(fun_time == 0){
    current = 0;
    seed_string = string(seed);
}

current += keyboard_check_pressed(vk_right) + keyboard_check_pressed(vk_left) * -1;
if(keyboard_check_pressed(vk_down)) current = 10;
if(keyboard_check_pressed(vk_up)) current = 0;
current = loop_number(current,0,13);

if(keyboard_check_pressed(vk_enter) && current < 10 && string_length(seed_string) < 12) seed_string += chr(48 + current);
if(keyboard_check_pressed(vk_enter) && current = 10) seed_string = string_copy(seed_string,0,string_length(seed_string)-1);
if(keyboard_check_pressed(vk_enter) && current = 11) seed_string = "";
if(keyboard_check_pressed(vk_enter) && current = 12){
    seed = real(seed_string);
    next_fun = game_menu;
    dir = right;
}

#define seed_menu_draw
draw_set_colour(c_gb_green);
draw_set_halign(fa_center);

var text = "current seed#" + seed_string;
draw_text(32,1,text);
for(var i = 0; i < 14; i++){
    draw_set_halign(fa_left);
    if(current == i) draw_set_color(c_gb_dk);
    else draw_set_color(c_gb_green);
    if(i < 10) draw_text(12 + i * 4, 17, string(i));
    if(i == 10) draw_text(12, 25, "del");
    if(i == 11) draw_text(27, 25, "clr");
    if(i == 12) draw_text(41, 25, "end");
}