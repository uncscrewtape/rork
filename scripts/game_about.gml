#define game_about
if(fun_time = 0){
    //Show about text.
    intro_slide = 0;
    fade = false;
    alph = 1;
}

alph = clamp(alph + (0.1 * !fade) + (-0.1 * fade),0 ,1);

if(alph == 0){
    fade = 0;
    intro_slide ++;
}

text = howto[intro_slide];

if(keyboard_check_pressed(vk_enter) && !fade && intro_slide < array_length_1d(howto) - 1) fade = 1;
if(keyboard_check_pressed(vk_enter) && intro_slide == array_length_1d(howto) -1) {
    next_fun = game_menu;
    dir = left;
}

#define game_about_draw
draw_set_alpha(alph);
draw_set_colour(c_gb_green);
draw_set_halign(fa_center);
draw_text_ext(32,1,text,6,62);
draw_set_alpha(1);
draw_text(32,55,"[Continue]");
//draw_sprite_ext(spr_bottom,0,32,63,1,1,0,c_gb_green,1);
