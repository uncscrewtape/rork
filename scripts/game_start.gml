#define game_start
if(fun_time = 0){
    random_set_seed(seed);
    ds_list_shuffle(room_descriptions);
    ds_list_shuffle(quest_items);
    my_rooms = ds_list_create(); //This will store the ids of the room objects.
    
    //Create the room objects with their descriptions.
    for(var i = 0; i < ds_list_size(room_descriptions); i++){
        var cur_room = instance_create(0,0,obj_room);
        cur_room.description = room_descriptions[| i];
        if(irandom(10) == 5){
            ds_list_add(cur_room.items, common_items[irandom(array_length_1d(common_items)-1)]);
        }
        if(irandom(10) == 6){
            ds_list_add(cur_room.items, common_items[irandom(array_length_1d(common_items)-1)]);
            ds_list_add(cur_room.items, common_items[irandom(array_length_1d(common_items)-1)]);
        }
        
        ds_list_add(my_rooms,cur_room);
    }
    
    dungeon = ds_grid_create(5,5);
    
    //Show intro text.
    intro_slide = 0;
    fade = false;
    alph = 1;
}

alph = clamp(alph + (0.1 * !fade) + (-0.1 * fade),0 ,1);

if(alph == 0){
    fade = 0;
    intro_slide ++;
}

text = intro[intro_slide];

if(keyboard_check_pressed(vk_enter) && !fade && intro_slide < array_length_1d(intro) - 1) fade = 1;
if(keyboard_check_pressed(vk_enter) && intro_slide == array_length_1d(intro) -1) {
    //Nothing at the moment.
}

#define game_start_draw
draw_set_alpha(alph);
draw_set_colour(c_gb_green);
draw_set_halign(fa_center);
draw_text_ext(32,1,text,6,62);
draw_set_alpha(1);
draw_text(32,55,"[Continue]");
//draw_sprite_ext(spr_bottom,0,32,63,1,1,0,c_gb_green,1);
