#define game_menu
if(fun_time == 0){
    menu[0,0] = "start";
    menu[1,0] = "seed";
    menu[2,0] = "about";
    menu[3,0] = "quit";
    
    menu[0,1] = mnu_start;
    menu[1,1] = mnu_seed;
    menu[2,1] = mnu_about;
    menu[3,1] = mnu_quit;
    
    current = 0;
    
    title = "welcome to rork";
}

current += keyboard_check_pressed(vk_up) * -1 + keyboard_check_pressed(vk_down);
current = loop_number(current,0,array_height_2d(menu));

if(keyboard_check_pressed(vk_enter) && script_exists(menu[current,1])) script_execute(menu[current,1]);

#define game_menu_draw
draw_set_colour(c_gb_green);
draw_set_halign(fa_center);

var text = title;
draw_text(32,1,text);

for(var i = 0; i < array_height_2d(menu); i ++){
    if(i == current) draw_set_color(c_gb_dk);
    else draw_set_colour(c_gb_green);
    draw_text(32,19 + i * 6,menu[i,0]);
}