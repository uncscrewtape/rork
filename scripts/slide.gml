///slide(direction);
var __dir;

__dir = argument[0];

if(fun_time == 0) {
    switch (__dir){
        case left:
            view_xview[0] = -64;
            view_yview[0] = 0;
        break;
        case right:
            view_xview[0] = 65;
            view_yview[0] = 0;
        break;
        case up:
            view_yview[0] = -64;
            view_xview[0] = 0;
        break;
        case down:
            view_yview[0] = 65;
            view_xview[0] = 0;
        break;
    }
}

switch (__dir){
    case left:
        view_xview[0] = min(view_xview[0] + 5, 0);
        view_yview[0] = 0;
    break;
    case right:
        view_xview[0] = max(view_xview[0] - 5, 0);
        view_yview[0] = 0;
    break;
    case up:
        view_yview[0] = min(view_yview[0] + 5, 0);
        view_xview[0] = 0;
    break;
    case down:
        view_yview[0] = max(view_yview[0] - 5, 0);
        view_xview[0] = 0;
    break;
}
