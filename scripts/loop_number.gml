///loop_number(current,min,max);
var __current, __min, __max;

__current = argument[0];
__min = argument[1];
__max = argument[2];

if(__current >= __max) return __min;
if(__current < __min) return __max - 1;
return __current;
