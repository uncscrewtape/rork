commands_init();
//Add commands, these will show up in the order you add them.
add_command("north",move_north);
add_command("south",move_south);
add_command("east",move_north);
add_command("west",move_south);
add_command("take",take_command);

room_descriptions = ds_list_create();
ds_list_add(room_descriptions,
"A damp dungeon room.",
"A clearing in a forest.",
"An old throne room.",
"An empty dining hall.",
"A dying garden.",
"A trashed bedroom.",
"A disgusting bathroom.",
"A crumbling barn.",
"A dark winding hallway.",
"A library.",
"Servants quarters.",
"An empty vault.",
"A dank and dark sewer.",
"An empty pantry.",
"A gently running stream."
);

common_items[0] = "bone";
common_items[1] = "rotten apple";
common_items[2] = "scrap of paper";
common_items[3] = "charred book";
common_items[4] = "unknown substance";

quest_items = ds_list_create();
ds_list_add(quest_items, "key", "ornate key", "torch", "book of spells");



//Intro story text.
intro[0] = "###You wake up in an unknown room...";
intro[1] = "###In an unknown kingdom...";
intro[2] = "###With no memory of what happened!..";
intro[3] = "###You must escape this place...";

//How to play.
howto[0] = "Welcome to rork.#A rougelike text based adventure. created by nate lewis";
howto[1] = "In rork, you navigate an unknown procedurally generated castle... Trying to get home.";
howto[2] = "Start by setting your seed from the main menu. Your castle will be generated based on this.";
howto[3] = "To navigate rork you will use the arrow keys. Left will open your inventory. Right will open the navigation options.";
howto[4] = "While the down arrow will open up the game menu.";
howto[5] = "thanks for playing and have fun!#Check out our other games here#ilovetojam.com";
